#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return r

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html',books=books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    return render_template('editBook.html', book_id=book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    cur_book = {}
    for oneBook in books:
        if int(oneBook['id']) == book_id:
            cur_book=oneBook
            print(cur_book)
    return render_template('deleteBook.html', cur_book=cur_book)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/edited/',methods=['GET','POST'])
def bookEdit(book_id):
    if request.method == 'POST':
        new_name = request.form['name']
        for book in books:
            if int(book['id']) == book_id:
                book['title'] = new_name
                print(book['id'])
                print(new_name)
    return redirect(url_for('showBook'))	

@app.route('/book/<int:book_id>/delete/deleted/',methods=['GET','POST']) 
def bookDelete(book_id):
    if request.method == 'POST':
        book_name = request.form['name']
        print(book_name)
        for book in books:
            if book['title'] == book_name:
                books.remove(book)
    return redirect(url_for('showBook'))

@app.route('/book/new/create/',methods=['GET','POST']) 
def bookNew():
    if request.method == 'POST':
        book_name = request.form['name']
        cur_id = int(books[0]['id'])
        index = 1
        while index < len(books) and cur_id == index:
            index += 1
            cur_id = int(books[index - 1]['id'])
            
        if cur_id == index:
            index+=1
        
        new_book = {'title': book_name, 'id': str(index)}
        books.insert(index - 1, new_book)
    return redirect(url_for('showBook'))

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)